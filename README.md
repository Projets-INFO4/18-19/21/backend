# Back-End
## Fonctionnalités implémentées
### Back 

* API fonctionnelle (cf document des routes)
* Possibilité de lancer l'apprentissage au démarrage du serveur
* Possibilité de remplir automatiquement la base de données au démarrage du serveur

### Deeplearning
* Mise en place de l'algorithme de deeplearning de word2Vector
* Mise en place de l'algorithme de deeplearning CNN pour générer le model de prediction
* Possibilité d'évaluer une offre en fonction d'un model de prédiction 


## Installation, lancement, déploiement

[Voir la page dédiée de la documentation](https://projets-info4.gricad-pages.univ-grenoble-alpes.fr/18-19/21/backend/).
