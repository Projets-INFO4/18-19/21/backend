# encoding: utf-8
# -*- coding: utf-8 -*-

"""
The flask application package.
"""

# parse arguments

from flask import Flask
from flask_cors import CORS
import argparse
import yaml
import os
import tensorflow as tf

directory = os.path.dirname(__file__)

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--testing', action='store_true')  # to use the testing database
parser.add_argument('-i', '--init', action='store_true')  # to use the testing database
parser.add_argument('-w', '--word2vec', action='store_true')  # to train word2vec model
args = parser.parse_known_args()


def get_path(filename):
    return os.path.join(directory, "../" + filename)


def set_config():
    """
    :return: a dictionary of programs parameters
    """
    with open(get_path("./config.yml"), 'r') as config_file:
        config = yaml.load(config_file)
        app.config = {**app.config, **config}


# remove arguments to not interfere with unittest
import sys
try:
    sys.argv.remove('-t')
except:
    pass
try:
    sys.argv.remove('--testing')
except:
    pass
try:
    sys.argv.remove('-i')
except:
    pass
try:
    sys.argv.remove('--init')
except:
    pass
try:
    sys.argv.remove('-r')
except:
    pass
try:
    sys.argv.remove("-w")
except:
    pass
try:
    sys.argv.remove("--word2vec")
except:
    pass


app = Flask(__name__)
app.config['TOKEN_SECRET'] = 'Secret_Token'  # Change this
app.config['SECRET_KEY'] = 'Secret_Key'  # Change this
app.config['CORS_HEADERS'] = ['Content-Type', 'Authorization']
app.config['CORS_AUTOMATIC_OPTIONS'] = True
CORS(app)

app.config['TESTING'] = args[0].testing
app.config['INIT'] = args[0].init
app.config['WORD2VEC'] = args[0].word2vec
set_config()

from SmartRecruiting_BackEnd.data import DatabaseManager
from SmartRecruiting_BackEnd.deeplearning.cnn.Model import Model
from SmartRecruiting_BackEnd.deeplearning.preprocess import preprocess

db_manager = DatabaseManager()

word2vec = preprocess.load_word2vec()

model = Model(save_dirname="./data/model", word2vec=word2vec, epochs=app.config["keras_model"]["epochs"])
model.load()
graph = tf.get_default_graph()

import SmartRecruiting_BackEnd.api.routes
import SmartRecruiting_BackEnd.data


