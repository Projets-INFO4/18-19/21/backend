# encoding: utf-8
# -*- coding: utf-8 -*-
"""
Routes and views for the flask application.

"""
from flask import jsonify, json, session, g
from flask import abort, request
from flask_cors import cross_origin
from functools import wraps
from bcrypt import gensalt, hashpw
from jwt import encode, decode, DecodeError, ExpiredSignature
from datetime import timedelta
from datetime import datetime
from SmartRecruiting_BackEnd import app, db_manager, graph, model


def create_token(user):
    """
    Create a token for a user with an expiration of 14 days
    """
    payload = {
        'sub': user.email.decode('utf-8'),
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = encode(payload, app.config['TOKEN_SECRET'])
    return token.decode('unicode_escape')


def parse_token(req):
    """
    Check if the token is correct
    """
    token = req.headers.get('Authorization').split()[1]
    return decode(token, app.config['TOKEN_SECRET'])


def login_required(f):
    """
    Decorator for allowing a logged in user to access to the route
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Allow OPTIONS request
        if request.headers.get("Access-Control-Request-Headers") == "authorization":
            response = jsonify(message="ok")
            response.status_code = 200
            return response

        # Reject request with no header Authorization
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            payload = parse_token(request)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_email = payload['sub']

        return f(*args, **kwargs)

    return decorated_function


def login_admin_required(f):
    """
    Decorator for allowing a logged in administrator to access to the route
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Allow OPTIONS request
        if request.headers.get("Access-Control-Request-Headers") == "authorization":
            response = jsonify(message="ok")
            response.status_code = 200
            return response

        # Reject request with no header Authorization
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            payload = parse_token(request)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response
        g.user_email = payload['sub']
        user = db_manager.user_manager.get_user_by_email(g.user_email)
        if user and user.is_admin:
            return f(*args, **kwargs)
        else:
            response = jsonify(message='Not admin')
            response.status_code = 401
            return response
    return decorated_function


@app.route('/users/<int:id_user>')
@cross_origin()
@login_admin_required
def get_user(id_user):
    """
    Function to get the user in the database
    :param id_user: int
    :return: user :{ "name" : str, "surname" : str, "role" : str, "email" : str, "password" : str, "is_admin" : bool }
    """
    user = db_manager.user_manager.get_user_by_id(id_user)
    if user is None:
        abort(404)
    else:
        return jsonify(user), 200


@app.route('/users/<int:id_user>', methods=['PUT'])
@cross_origin()
@login_required
def update_user(id_user):
    """
    METHOD : PUT
    HEADER PARAM  : id_user: int
    BODY PARAMS : { "name" : str, "surname" : str, "role" : str, "email" : str, "password" : str,"is_admin" : bool }
    """
    data = json.loads(request.data)
    name = data.get('name', None)
    surname = data.get('surname', None)
    role = data.get('role', None)
    email = data.get('email', None)
    password = data.get('password', None)
    if password is not None:
        password = hashpw(password.encode('utf-8'), gensalt())
    print(password)
    is_admin = data.get('is_admin', None)
    response = db_manager.user_manager.update_user(id_user, name, surname, role, email, password, is_admin)
    if response is None:
        abort(404)
    else:
        if response:
            return '', 200
        else:
            abort(400)


@app.route('/users/<int:id_user>', methods=['DELETE'])
@cross_origin()
@login_required
def delete_user(id_user):
    """
    METHOD : DELETE
    HEADER PARAM  : id_user: int
    """
    if db_manager.user_manager.delete_user(id_user) is None:
        abort(404)
    else:
        return '', 200


@app.route('/offers')
@cross_origin()
@login_admin_required
def get_offers():
    """
    Function to get all the offers in the database
    :return: {"offer":{ "title" : str, "content" : str, "descriptor" : str, "id_user" : int }}
    """
    return jsonify(db_manager.offer_manager.get_all_offers()), 200


@app.route('/offers/<int:id_offer>')
@cross_origin()
@login_required
def get_offer(id_offer):
    """
    Get the offer of id id_offer
    :param id_offer: int
    :return: offer:{ "title" : str, "content" : str, "descriptor" : str, "id_user" : int }
    """
    offer = db_manager.offer_manager.get_offer_by_id(id_offer)
    if offer is None:
        abort(404)
    else:
        return jsonify(offer), 200


@app.route('/offers', methods=['POST'])
@cross_origin()
@login_admin_required
def add_offer():
    """
    Function to add an offer in the database
    METHOD : POST
    HEADER PARAM  : None
    BODY PARAMS : { "title" : str, "content" : str, "descriptor" : str, "id_user" : int }
    """
    data = json.loads(request.data)
    if db_manager.offer_manager.add_offer(data['title'], data['content'], data['id_user']):
        return '', 201
    else:
        abort(400)


@app.route('/offers/link', methods=['POST'])
@cross_origin()
@login_required
def add_offer_link_field():
    """
    Function to add an offer in the database and link it to a field
    METHOD : POST
    HEADER PARAM  : None
    BODY PARAMS : { "title" : str, "content" : str, "id_user" : int, "dic_fields" : dictionary{id_field, mark}, "inbase" : bool }
    """
    data = json.loads(request.data)
    print(data)
    id_offer = db_manager.offer_manager.add_offer_v2(data['title'], data['content'], data['id_user'])
    for id_field, mark in data['dic_fields'].items():
        is_inbase = db_manager.prediction_manager.add_prediction(mark["note"], mark["id"], id_offer, data['inbase'])
    if is_inbase:
        return jsonify(is_inbase), 201
    else:
        abort(400)


@app.route('/offers/<int:id_offer>', methods=['PUT'])
@cross_origin()
@login_required
def update_offer(id_offer):
    """
    Function to update an offer
    METHOD : PUT
    HEADER PARAM  : id_offer :int
    BODY PARAMS : { "title" : str, "content" : str, "descriptor" : str, "id_user" : int }
    """
    data = json.loads(request.data)
    title = data.get('title', None)
    content = data.get('content', None)
    id_user = data.get('id_user', None)

    response = db_manager.offer_manager.update_offer(id_offer, title, content, id_user)
    if response is None:
        abort(404)
    else:
        if response:
            return '', 200
        else:
            abort(400)


@app.route('/offers/<int:id_offer>', methods=['DELETE'])
@cross_origin()
# @loginRequired
def delete_offer(id_offer):
    """
    METHOD : DELETE
    HEADER PARAM  : id_offer :int
    """
    if db_manager.offer_manager.delete_offer(id_offer) is None:
        abort(404)
    else:
        return '', 200


@app.route('/fields')
@cross_origin()
# @loginAdminRequired
def get_fields():
    """
    Function to get all the field in the database
    :return: {"fields":{"name": str, "description": str, "descriptor": str,"website": str}}
    """
    def complete_field(f):
        f['contacts'] = db_manager.field_manager.get_field_contacts(f['id'])
        f['offers'] = db_manager.field_manager.get_field_offers(f['id'])
        return f

    fields = db_manager.field_manager.get_all_fields()
    if fields:
        fields = [complete_field(f) for f in fields]
    else:
        abort(404)
    return jsonify(fields), 200


@app.route('/fields/nameonly')
@cross_origin()
def get_fields_name():
    """
    Function to get all the field in the database
    :return: {"fields":{"id":int, "name": str}}
    """
    return jsonify(db_manager.field_manager.get_all_fields_name()), 200


@app.route('/fields/<int:id_field>')
@cross_origin()
def get_field(id_field):
    """
    Function to get a field in the database
    :param id_field: int
    :return: {"id": int, "name": str, "description": str, "descriptor": str,"website": str, "contacts":}
    """
    field = db_manager.field_manager.get_field_by_id(id_field)
    # print(field)
    if field is None:
        abort(404)
    else:
        field["contacts"] = db_manager.field_manager.get_field_contacts(field['id'])
        field['offers'] = db_manager.field_manager.get_field_offers(field['id'])
        return jsonify(field), 200


@app.route('/fields', methods=['POST'])
@cross_origin()
@login_admin_required
def add_field():
    """
    Function to add a field in the database
    :METHOD : POST
    :HEADER PARAM  : None
    :BODY PARAMS :{"name": str, "description": str, "descriptor": str,"website": str, "contacts":
        [{"name": str, "surname": str, "email": str,"phone": str,"role": str,"id_field": int}, ...]
    }
    """
    data = json.loads(request.data)
    field = db_manager.field_manager.add_field(data['name'], data['description'], data['website'])
    if field is not None:
        field['contacts'] = []
        for contact in data['contacts']:
            created_contact = db_manager.contact_manager.add_contact(contact['name'], contact['surname'], contact['email'], contact['phone'], contact['role'], field['id'])
            if created_contact:
                field['contacts'].append(created_contact)
            else:
                abort(400)
        return jsonify(field), 201
    else:
        abort(400)


@app.route('/fields/<int:id_field>', methods=['PUT'])
@cross_origin()
@login_admin_required
def update_field(id_field):
    """
    Function to update a field in the database
    :METHOD : PUT
    :HEADER PARAM  : id_field : int
    :BODY PARAMS :{"name": str, "description": str, "descriptor": str,"website": str}
    """
    data = json.loads(request.data)
    name = data.get('name', None)
    description = data.get('description', None)
    website = data.get('website', None)
    response = db_manager.field_manager.update_field(id_field, name, description, website)
    if response is None:
        abort(404)
    else:
        if response:
            return '', 200
        else:
            abort(400)


@app.route('/fields/<int:id_field>', methods=['DELETE'])
@cross_origin()
@login_admin_required
def delete_field(id_field):
    """
    Function to delete a field in the database
    :METHOD : DELETE
    :HEADER PARAM  : id_field : int
    """
    if db_manager.field_manager.delete_field(id_field) is None:
        abort(404)
    else:
        return '', 200


@app.route('/contacts')
@cross_origin()
@login_admin_required
def get_contacts():
    """
    Function to get all the contact in the database
    :return: {"contact":{"name": str, "surname": str, "email": str,"phone": str,"role": str,"id_field": int}}
    """
    return jsonify(db_manager.contact_manager.get_all_contacts()), 200


@app.route('/contacts/<int:id_contact>')
@cross_origin()
def get_contact(id_contact):
    """
    Function to get a contact in the database
    :param id_contact: int
    :return: {"name": str, "surname": str, "email": str,"phone": str,"role": str,"id_field": int}
    """
    contact = db_manager.contact_manager.get_contact_by_id(id_contact)
    if contact is None:
        abort(404)
    else:
        return jsonify(contact), 200


@app.route('/contacts', methods=['POST'])
@cross_origin()
@login_admin_required
def add_contact():
    """
    Function to add a contact in the database
    :METHOD : POST
    :HEADER PARAM  : None
    :BODY PARAMS :{"name": str, "surname": str, "email": str,"phone": str,"role": str,"id_field": int}
    """

    data = json.loads(request.data)
    email = data.get('email', None)
    phone = data.get('phone', None)
    role = data.get('role', None)
    contact = db_manager.contact_manager.add_contact(data['name'], data['surname'], email, phone, role, data['id_field'])
    if contact is not None:
        return jsonify(contact), 201
    else:
        abort(400)


@app.route('/contacts/<int:id_contact>', methods=['PUT'])
@cross_origin()
@login_admin_required
def update_contact(id_contact):
    """
    Function to update a contact in the database
    :METHOD : PUT
    :HEADER PARAM  : id_contact : int
    :BODY PARAMS :{"name": str, "surname": str, "email": str,"phone": str,"role": str,"id_field": int}
    """
    data = json.loads(request.data)
    name = data.get('name', None)
    surname = data.get('surname', None)
    email = data.get('email', None)
    phone = data.get('phone', None)
    role = data.get('role', None)
    id_field = data.get('id_field', None)
    response = db_manager.contact_manager.update_contact(id_contact, name, surname, email, phone, role, id_field)
    if response is None:
        abort(404)
    else:
        if response:
            return '', 200
        else:
            abort(400)


@app.route('/contacts/<int:id_contact>', methods=['DELETE'])
@cross_origin()
@login_admin_required
def delete_contact(id_contact):
    """
    Function to delete a contact in the database
    :METHOD : DELETE
    :HEADER PARAM  : id_contact : int
    """

    if db_manager.contact_manager.delete_contact(id_contact) is None:
        abort(404)
    else:
        return '', 200


@app.route('/generatePrediction', methods=['POST'])
@cross_origin()
def generate_prediction():
    """
    Function to get a prediction from an offer
    METHOD : POST
    HEADER PARAM  : None
    BODY PARAMS : { "title" : str, "content" : str }
    RETURNS :
        {
            "field": { "name": str, "description": str, "descriptor": str, "website": str }
        }
    """
    with graph.as_default():
        data = json.loads(request.data)
        print("-------------------------------------JSON data ----------------------------------------")
        print(data['content'])

        predictions = model.predict(data['content'])
        print(predictions)
        res = {}
        for prediction, note in predictions.items():
            field = db_manager.field_manager.get_field_by_id(prediction)
            if field is not None:
                field["note"] = int(note * 100)
                res[prediction] = field

        if None is not None:
            abort(404)
        else:
            print(jsonify(res))
            return jsonify(res), 200


@app.route('/searchOffersByUser/<int:id_user>', methods=['GET'])
@cross_origin()
def offers_by_user(id_user):
    """
    Function to get all the offers who correspond to an user
    :METHOD : GET
    :HEADER PARAM  : id_user : int
    :BODY PARAMS : none
    :return: {"offer":{ "title" : str, "content" : str, "descriptor" : str, "id_user" : int }}
    """
    offers = db_manager.offer_manager.offers_by_user(id_user)
    if offers is None:
        abort(404)
    else:
        return jsonify(offers), 200


@app.route('/searchFieldsByOffer/<int:id_offer>', methods=['GET'])
@cross_origin()
@login_required
def fields_by_offer(id_offer):
    """
    Function to get all the field who correspond to an offer
    :METHOD : GET
    :HEADER PARAM  : id_offer : int
    :BODY PARAMS : none
    :return: {"fields":{"id": int,"name": str,"inbase": boolean}}
    """
    res = {}
    dic = db_manager.prediction_manager.fields_by_offer(id_offer)
    for d in dic:
        # d[0] = id_field | d[1] = mark
        field = db_manager.field_manager.get_field_by_id(d[0])
        if field is not None:
            field["note"] = d[1]
            res[d[0]] = field

    return jsonify(res), 200


@app.route('/nbPrediction', methods=['POST'])
@cross_origin()
@login_admin_required
def nb_prediction():
    """
    Function to get the number of prediction between two date
    :METHOD : GET
    :HEADER PARAM  : none
    :BODY PARAMS : {"begin_date": date,"end_date":date}
    :return: int
    """
    data = json.loads(request.data)
    begin_date = data.get('begin_date', None)
    end_date = data.get('end_date', None)
    begin_date = datetime.strptime(begin_date, "%Y-%m-%d").date()
    end_date = datetime.strptime(end_date, "%Y-%m-%d").date()
    number = db_manager.prediction_manager.nb_prediction(begin_date, end_date)
    if number is None:
        abort(404)
    else:
        return jsonify(number), 200


@app.route('/accuracy', methods=['GET'])
@cross_origin()
@login_admin_required
def get_accuracy():
    """
    Function to get the accuracy of the system
    :METHOD : GET
    :HEADER PARAM  : None
    :BODY PARAMS : None
    :return: int
    """
    nb_test, accuracy = None, None

    if nb_test is None or accuracy is None:
        abort(404)
    else:
        return jsonify(nb_test, accuracy), 200


# AUTHETIFICATION
@app.route('/update_prediction_by_id_offer', methods=['POST'])
@cross_origin()
@login_required
def update_prediction_by_id_offer():
    """
    Function to update the prediction corresponding to an offer
    :METHOD : POST
    :HEADER PARAM  : none
    :BODY PARAMS : { "id_offer" : int, "id_field" : int }
    :return: int
    """
    data = json.loads(request.data)
    in_base = data.get('in_base', None)
    id_field = data.get('id_field', None)
    if db_manager.prediction_manager.update_prediction_by_id_offer(data['id_offer'], id_field, in_base):
        return '', 201
    else:
        abort(400)


# AUTHENTIFICATION
@app.route('/auth/signup', methods=['POST'])
@cross_origin()
def signup():
    """
    METHOD : POST
    HEADER PARAM  : None
    BODY PARAMS : { "name" : str, "surname" : str, "role" : str, "email" : str, "password" : str }
    RETURNS :
        {
            "token": str,
            "user": { "email": str, "id": int, "is_admin": boolean, "name": str,
                        "password": str, "role": str, "surname": str }
        }
    """
    data = json.loads(request.data)

    user = db_manager.user_manager.get_user_by_email(data["email"])

    if user is not None:
        return jsonify(error="User already exist"), 404

    password = hashpw(data["password"].encode('utf-8'), gensalt())

    if db_manager.user_manager.add_user(data["name"], data["surname"], data["role"], data['email'], password, False):
        session['logged_in'] = True
        user = db_manager.user_manager.get_user_by_email(data["email"])
        return jsonify(user=user.serialize(), token=create_token(user)), 200
    else:
        abort(400)


@app.route('/auth/login', methods=['POST'])
@cross_origin()
def login():
    """
    METHOD : POST
    HEADER PARAM  : None
    BODY PARAMS : { "emailUser" : str, "password" : str}
    RETURNS :
        {
            "token": str,
            "user": { "email": str, "id": int, "is_admin": boolean, "name": str,
                        "password": str, "role": str, "surname": str }
        }
    """
    data = json.loads(request.data)

    user = db_manager.user_manager.get_user_by_email(data["emailUser"])

    if not user:
        return jsonify(error="No such user"), 404

    password = data["password"].encode('utf-8')

    if user.password == hashpw(password, user.password):
        session['logged_in'] = True
        return jsonify(user=user.serialize(), token=create_token(user)), 200
    else:
        return jsonify(error="Wrong name or password"), 400


@app.route('/auth/logout', methods=['POST'])
@cross_origin()
@login_required
def logout():
    """
    METHOD : POST
    HEADER PARAM  : {Authorization : Bearer token}
    BODY PARAMS : None
    RETURNS : { 'result': 'success' }
    """
    session.pop('logged_in', None)
    return jsonify({'result': 'success'}), 200
