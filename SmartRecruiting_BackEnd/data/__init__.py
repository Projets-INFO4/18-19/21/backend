# encoding: utf-8
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 11:09:56 2017

Pistes d'amélioration : https://www.tensorflow.org/guide/using_gpu

Useful resources
=================

* `Getting started with the Keras Sequential model <https://keras.io/getting-started/sequential-model-guide/>`
* `Save and restore models <https://www.tensorflow.org/tutorials/keras/save_and_restore_models>`_
* `Keras - FAQ <https://keras.io/getting-started/faq/#savingloading-whole-models-architecture-weights-optimizer-state>`_
* `Train your first neural network: basic classification <https://www.tensorflow.org/tutorials/keras/basic_classification>`_

:Authors: Romain Guillot , Julian
"""

from SmartRecruiting_BackEnd.data.database import *
from SmartRecruiting_BackEnd.data.models import *
from SmartRecruiting_BackEnd.data.manager import *
