from SmartRecruiting_BackEnd.data.database import dbSession as dB
from SmartRecruiting_BackEnd.data.models import Contact


class ContactManager:

    @staticmethod
    def get_all_contacts():
        """
        Obtain all contacts

        :return: list contact serialize
        """
        contacts = Contact.query.all()
        return [c.serialize() for c in contacts]

    @staticmethod
    def get_contact_by_id(id_contact):
        """

        :param id_contact: id of a contact
        :type id_contact: int

        :return: Contact serialize
        """
        contact = Contact.query.get(id_contact)
        if contact is None:
            return None
        else:
            return contact.serialize()

    @staticmethod
    def add_contact(name, surname, email, phone, role, id_field):
        """
        Add contact in database

        :param name: name of a contact
        :type name: str

        :param surname: surname of a contact
        :type surname: str

        :param email: email of a contact
        :type email: str

        :param phone: phone of a contact
        :type phone: str

        :param role: role of a contact
        :type role: str

        :param id_field: id of a field
        :type id_field: int

        :return: Contact serialize
        """
        contact = Contact(name, surname, email, phone, role, id_field)
        dB.add(contact)
        try:
            dB.commit()
            return contact.serialize()
        except Exception:
            dB.rollback()
            return None

    @staticmethod
    def update_contact(id_contact, name, surname, email, phone, role, id_field):
        """
        Update contact in database

        :param id_contact: id of a contact
        :type id_contact: int

        :param name: name of a contact
        :type name: str

        :param surname: surname of a contact
        :type surname: str

        :param email: email of a contact
        :type email: str

        :param phone: phone of a contact
        :type phone: str

        :param role: role of a contact
        :type role: str

        :param id_field: id of a field
        :type id_field: int

        :return: Boolean if contact was updated
        """
        contact = Contact.query.get(id_contact)
        if contact is None:
            return None
        else:
            try:
                if name is not None:
                    contact.name = name
                if surname is not None:
                    contact.surname = surname
                if email is not None:
                    contact.email = email
                if phone is not None:
                    contact.phone = phone
                if role is not None:
                    contact.role = role
                if id_field is not None:
                    contact.id_field = id_field
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False

    @staticmethod
    def delete_contact(id_contact):
        """
        Delete contact in database

        :param id_contact: id of a contact
        :type id_contact: int

        :return: Boolean if contact was deleted
        """
        contact = Contact.query.get(id_contact)
        if contact is None:
            return None
        else:
            dB.delete(contact)
            dB.commit()
            return True
