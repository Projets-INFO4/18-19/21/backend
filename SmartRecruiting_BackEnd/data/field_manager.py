from SmartRecruiting_BackEnd.data.models import Field
from SmartRecruiting_BackEnd.data.database import dbSession as dB
from SmartRecruiting_BackEnd.data.contact_manager import ContactManager
from SmartRecruiting_BackEnd.data.prediction_manager import PredictionManager


class FieldManager:

    @staticmethod
    def get_all_fields():
        """

        :return: dict
        """
        fields = Field.query.all()
        return [p.serialize() for p in fields]

    @staticmethod
    def get_all_fields_name():
        """

        :return: list of dict {'id' : int ,'name' : str}
        """
        fields = Field.query.with_entities(Field.id, Field.name).all()
        result = []
        for f in fields:
            result.append({'id': f[0], 'name': f[1].decode("utf-8")})
        return result

    @staticmethod
    def get_field_by_id(id_field):
        """

        :param id_field: id of a field
        :type id_field: int

        :return: dict
        """
        field = Field.query.get(id_field)
        if field is None:
            return None
        else:
            return field.serialize()

    @staticmethod
    def get_field_by_name(name):
        """

        :param name: name of a field
        :type name: str

        :return: Field obtain by name
        """
        return Field.query.filter_by(name=name).first()

    @staticmethod
    def get_all_id_field():
        """

        :return: Dictionary where each element is a field
        """
        field = Field.query \
            .with_entities(Field.id)
        return field

    @staticmethod
    def get_field_contacts(id_field):
        """
        Obtain all contact thanks to an id of a field

        :param id_field: id of a field
        :type id_field: int

        :return: list of contacts serialize
        """
        field = Field.query.get(id_field)
        return [c.serialize() for c in field.contacts]

    @staticmethod
    def get_field_offers(id_field):
        """
        Obtain all offers thanks to an id of a field

        :param id_field: id of a field
        :type id_field: int

        :return: list of offers serialize
        :rtype: list of dict
        """
        field = Field.query.get(id_field)
        return [o.serialize() for o in field.offers]

    @staticmethod
    def add_field(name, description, website):
        """
        Add field in database

        :param name: name of a field
        :type name: str

        :param description: description of a field
        :type description: str

        :param website: website of a field
        :type website: str

        :return: field serialize
        """
        field = Field(name, description, website)
        dB.add(field)
        try:
            dB.commit()
            return field.serialize()
        except Exception:
            dB.rollback()
            return None

    @staticmethod
    def add_field_v2(name, description, website):
        """
         Add field in database

         :param name: name of a field
         :type name: str

         :param description: description of a field
         :type description: str

         :param website: website of a field
         :type website: str

         :return: id_field added in database or -1
         """
        field = Field(name, description, website)
        dB.add(field)
        try:
            dB.commit()
            return field.id
        except Exception:
            dB.rollback()
            return -1

    @staticmethod
    def update_field(id_field, name, description, website):
        """
        Update field in database

        :param id_field: if of a field
        :type id_field: int

        :param name: name of a field
        :type name: str

        :param description: description of a field
        :type description: str

        :param website: website of a field
        :type website: str

        :return: Boolean if field was updated
        """
        field = Field.query.get(id_field)
        if field is None:
            return None
        else:
            try:
                if name is not None:
                    field.name = name
                if description is not None:
                    field.description = description
                if website is not None:
                    field.website = website
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False

    @staticmethod
    def delete_field(id_field):
        """
        Delete field in database

        :param id_field: id of a field
        :type id_field: int

        :return: Boolean if field was deleted
        """
        field = Field.query.get(id_field)
        if field is None:
            return None
        else:
            for contact in field.contacts:
                ContactManager.delete_contact(contact.id)

            PredictionManager.delete_prediction_by_id_field(id_field)

            dB.delete(field)
            dB.commit()
            return True
