# encoding: utf-8
# -*- coding: utf-8 -*-
"""
Manager to interact with the database

Created on Sun Feb 26 16:23:02 2017
@author: Julian
"""
from SmartRecruiting_BackEnd.data.user_manager import UserManager
from SmartRecruiting_BackEnd.data.offer_manager import OfferManager
from SmartRecruiting_BackEnd.data.field_manager import FieldManager
from SmartRecruiting_BackEnd.data.contact_manager import ContactManager
from SmartRecruiting_BackEnd.data.prediction_manager import PredictionManager
from SmartRecruiting_BackEnd.data.database import init_db
from SmartRecruiting_BackEnd import app
from SmartRecruiting_BackEnd.deeplearning.cnn.train import train
from SmartRecruiting_BackEnd.deeplearning.preprocess.preprocess import init


class DatabaseManager:

    def __init__(self):
        """
            initialize the database, creates tables if not exists
        """
        self.user_manager = UserManager()
        self.offer_manager = OfferManager()
        self.field_manager = FieldManager()
        self.contact_manager = ContactManager()
        self.prediction_manager = PredictionManager()

        build_word2vec = app.config['WORD2VEC']
        if app.config['INIT']:
            init_db()
            if self.user_manager.get_one_admin() is None:
                self.user_manager.add_user("monsieur", "administrateur", "admin", "admin@", "root", 1)

            init(self, generate_w2c=build_word2vec)
            train(self)
