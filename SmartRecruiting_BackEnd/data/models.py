# encoding: utf-8
# -*- coding: utf-8 -*-
"""
ORM representation of tables


Created on Fri Feb 24 22:44:09 2017

@author: Julian
"""

from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import LONGTEXT
from SmartRecruiting_BackEnd.data.database import Base
from SmartRecruiting_BackEnd import app

long_text = LONGTEXT
if app.config['TESTING']:
    long_text = Text


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(100), unique=False, nullable=False)
    surname = Column(String(100), unique=False, nullable=False)
    role = Column(String(100), unique=False, nullable=True)
    email = Column(String(100), unique=True, nullable=False)
    password = Column(String(100), unique=False, nullable=False)
    is_admin = Column(Boolean, unique=False, nullable=False)
    offers = relationship("Offer")

    def __init__(self, name, surname, role, email, password, is_admin):
        self.name = name
        self.surname = surname
        self.role = role
        self.email = email
        self.password = password
        self.is_admin = is_admin


class Prediction(Base):
    __tablename__ = 'prediction'
    id_offer = Column(Integer, ForeignKey('offer.id'), primary_key=True)
    id_field = Column(Integer, ForeignKey('field.id'), primary_key=True)
    mark = Column(Float)
    is_verify = Column(Boolean, unique=False, nullable=False)
    field = relationship("Field", back_populates="offers")
    offer = relationship("Offer", back_populates="fields")

    def __init__(self, mark, id_field, id_offer, is_verify):
        self.mark = mark
        self.id_field = id_field
        self.id_offer = id_offer
        self.is_verify = is_verify


class Offer(Base):
    __tablename__ = 'offer'
    id = Column(Integer, primary_key=True, nullable=False)
    title = Column(String(100), unique=False, nullable=False)
    content = Column(Text, unique=False, nullable=False)
    id_user = Column(Integer, ForeignKey('user.id'), unique=False, nullable=False)
    fields = relationship("Prediction", back_populates="offer")

    def __init__(self, title, content, id_user):
        self.title = title
        self.content = content
        self.id_user = id_user


class Field(Base):
    __tablename__ = 'field'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(500), unique=True, nullable=False)
    description = Column(Text, unique=False, nullable=True)
    website = Column(String(500), unique=False, nullable=True)
    contacts = relationship("Contact")
    offers = relationship("Prediction", back_populates="field")

    def __init__(self, name, description, website):
        self.name = name
        self.description = description
        self.website = website


class Contact(Base):
    __tablename__ = 'contact'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(100), unique=False, nullable=False)
    surname = Column(String(100), unique=False, nullable=False)
    email = Column(String(100), unique=False, nullable=True)
    phone = Column(String(20), unique=False, nullable=True)
    role = Column(String(100), unique=False, nullable=True)
    id_field = Column(Integer, ForeignKey('field.id'), unique=False, nullable=False)

    def __init__(self, name, surname, email, phone, role, id_field):
        self.name = name
        self.surname = surname
        self.email = email
        self.phone = phone
        self.role = role
        self.id_field = id_field
