from SmartRecruiting_BackEnd.data.models import Offer, Prediction
from SmartRecruiting_BackEnd.data.database import dbSession as dB
from SmartRecruiting_BackEnd.data.prediction_manager import PredictionManager


class OfferManager:

    @staticmethod
    def get_all_offers():
        """

        :return: All offer in the database
        :rtype: list
        """
        offers = Offer.query.all()
        return [o.serialize() for o in offers]

    @staticmethod
    def get_offer_by_id(id_offer):
        """

        :param id_offer: id of an offer
        :type id_offer: int

        :return: Offer with id_offer
        :rtype: Offer serialize
        """
        offer = Offer.query.get(id_offer)
        if offer is None:
            return None
        else:
            return offer.serialize()

    @staticmethod
    def add_offer(title, content, id_user):
        """
        Add offer in database

        :param title: title of an offer
        :type title: str

        :param content: content of an offer
        :type content: str

        :param id_user: id of a user
        :type id_user: int

        :return: Boolean if the offer was added in database
        :rtype: Boolean
        """
        offer = Offer(title, content, id_user)
        dB.add(offer)
        try:
            dB.commit()
            return True
        except Exception:
            dB.rollback()
            return False

    @staticmethod
    def add_offer_v2(title, content, id_user):
        """
        Add offer in database

        :param title: title of an offer
        :type title: str

        :param content: content of an offer
        :type content: str

        :param id_user: id of a user
        :type id_user: int

        :return: id of the offer added in database
        :rtype: int
        """
        offer = Offer(title, content, id_user)
        dB.add(offer)
        try:
            dB.commit()
            return offer.id
        except Exception:
            dB.rollback()
            return -1

    @staticmethod
    def update_offer(id_offer, title, content, id_user):
        """
        Update an offer in database

        :param id_offer: id of an offer
        :type id_offer: int

        :param title: title of an offer
        :type title: str

        :param content: content of an offer
        :type content: str

        :param id_user: id of a user
        :type id_user: int

        :return: Boolean if the offer was updated in database
        :rtype: Boolean
        """
        offer = Offer.query.get(id_offer)
        if offer is None:
            return None
        else:
            try:
                if title is not None:
                    offer.title = title
                if content is not None:
                    offer.content = content
                if id_user is not None:
                    offer.id_user = id_user
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False

    @staticmethod
    def delete_offer(id_offer):
        """
        Delete an offer in database

        :param id_offer: id of an offer
        :type id_offer: int

        :return: Boolean if the offer was deleted in database
        :rtype: Boolean
        """
        offer = Offer.query.get(id_offer)
        if offer is None:
            return None
        else:
            PredictionManager.delete_prediction_by_id_offer(id_offer)

            dB.delete(offer)
            dB.commit()
            return True

    @staticmethod
    def get_all_offers_with_field_in_base():
        """
        Select all verify Prediction, i. e. the mark is 100 and the associate field (id_field) is a reference for an
        offer (id_offer)

        :return: Dictionary where each element is a list, in this list the first element is content of an offer and the second is id of field
        :rtype: Dictionary
        """
        #  Renvoyer un tableau[2] : [0] -> content offer | [1] -> id field pour chaque offre ayant is_verify True
        offers = Offer.query \
            .with_entities(Offer.content, Prediction.id_field) \
            .join(Prediction, Offer.id == Prediction.id_offer) \
            .filter_by(is_verify=True)

        return offers

    @staticmethod
    def offers_by_user(id_user):
        """

        :param id_user: id of a user
        :type id_user: int

        :return: Offer with id_user associate
        :rtype: Offer serialize
        """
        offers = Offer.query \
            .filter(Offer.id_user == id_user)
        return [o.serialize() for o in offers]
