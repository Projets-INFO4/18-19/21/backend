from sqlalchemy.sql import func
from SmartRecruiting_BackEnd.data.database import dbSession as dB
from SmartRecruiting_BackEnd.data.models import Prediction, Offer


class PredictionManager:

    @staticmethod
    def add_prediction(mark, id_field, id_offer, is_verify):
        """
        Add a prediction in database

        :param mark: mark of a prediction
        :type mark: int

        :param id_field: id of a field
        :type id_field: int

        :param id_offer: id of an offer
        :type id_offer: int

        :param is_verify: boolean if the formation is a reference for an offer
        :type is_verify: bool

        :return: boolean if the prediction was added in database
        :rtype: bool
        """
        prediction = Prediction(mark, id_field, id_offer, is_verify)
        dB.add(prediction)
        try:
            dB.commit()
            return True
        except Exception as e:
            dB.rollback()
            print(e)
            return False

    @staticmethod
    def delete_prediction_by_id_field(id_field):
        """
        Delete a prediction with an id_field

        :param id_field: id of a field
        :type id_field: int

        :return: Boolean if the prediction was deleted in database
        :rtype: bool
        """
        Prediction.query.filter_by(id_field=id_field).delete()
        try:
            dB.commit()
            return True
        except Exception:
            dB.rollback()
            return False

    @staticmethod
    def delete_prediction_by_id_offer(id_offer):
        """
        Delete a prediction with an id_offer

        :param id_offer: id of an offer
        :type id_offer: int

        :return: Boolean if the prediction was deleted in database
        :rtype: bool
        """
        Prediction.query.filter_by(id_offer=id_offer).delete()
        try:
            dB.commit()
            return True
        except Exception:
            dB.rollback()
            return False

    @staticmethod
    def fields_by_offer(id_offer):
        """
        Obtain id_field and mark of all prediction having this id_offer

        :param id_offer: id of an offer
        :type id_offer: int

        :return: Dictionary where each element is a list, in this list the first element is id_field and the second is a mark
        :rtype: dict
        """
        predictions = Prediction.query \
            .with_entities(Prediction.id_field, Prediction.mark)\
            .filter_by(id_offer=id_offer)
        return predictions

    """
    Not up to date with the new database model
    TODO Delete this route in routes.py (backend) and frontend.
    """
    @staticmethod
    def update_prediction_by_id_offer(id_offer, id_field, is_verify):
        offer = Offer.query.get(id_offer)
        if offer is None:
            return None
        else:
            try:
                prediction = offer.prediction
                if id_field is not None:
                    teams = prediction.teams
                    for team in teams:
                        team.id_field = id_field
                if is_verify is not None:
                    prediction.inbase = (is_verify == 1)
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False

    """
    Not up to date with the new database model
    TODO Delete this route in routes.py (backend) and frontend.
    """
    @staticmethod
    def nb_prediction(begin_date, end_date):
        nb_prediction = Prediction.query \
            .with_entities(func.count(Prediction.id).label('number')) \
            .filter(Prediction.date <= end_date, Prediction.date >= begin_date)\
            .first().number
        return nb_prediction
