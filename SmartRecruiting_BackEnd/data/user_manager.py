from SmartRecruiting_BackEnd.data.models import User
from SmartRecruiting_BackEnd.data.database import dbSession as dB
from SmartRecruiting_BackEnd.data.offer_manager import OfferManager


class UserManager:

    @staticmethod
    def get_one_admin():
        """
        :return: Return the first admin find in the database
        :rtype: User
        """
        return User.query.filter_by(is_admin=1).first()

    @staticmethod
    def get_user_by_id(id_user):
        """
        :param id_user: id of a user
        :type id_user: int

        :return: User obtain with id_user
        :rtype: dict
        """
        user = User.query.get(id_user)
        if user is None:
            return None
        else:
            return user.serialize()

    @staticmethod
    def get_user_by_email(email):
        """
        :param email:
        :type email: str

        :return: User obtain with email parameter
        :rtype: User
        """
        return User.query.filter_by(email=email).first()

    @staticmethod
    def add_user(name, surname, role, email, password, is_admin):
        """
        Add user in database

        :param name:
        :type name: str

        :param surname:
        :type surname: str

        :param role:
        :type role: str

        :param email:
        :type email: str

        :param password:
        :type password: str

        :param is_admin:
        :type is_admin: bool

        :return: Boolean if the user was added in database
        :rtype: bool
        """
        user = User(name, surname, role, email, password, is_admin == 1)
        dB.add(user)
        try:
            dB.commit()
            return True
        except Exception:
            dB.rollback()
            return False

    @staticmethod
    def update_user(id_user, name, surname, role, email, password, is_admin):
        """
        Update user in database

        :param id_user:
        :type id_user: int

        :param name:
        :type name: str

        :param surname:
        :type surname: str

        :param role:
        :type role: str

        :param email:
        :type email: str

        :param password:
        :type password: str

        :param is_admin:
        :type is_admin: bool

        :return: Boolean if the user was updated in database
        :rtype: bool
        """
        user = User.query.get(id_user)

        if user is None:
            return None
        else:
            try:
                if name is not None:
                    user.name = name
                if surname is not None:
                    user.surname = surname
                if role is not None:
                    user.role = role
                if email is not None:
                    user.email = email
                if password is not None:
                    user.password = password
                if is_admin is not None:
                    user.is_admin = is_admin == 1
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False

    @staticmethod
    def delete_user(id_user):
        """
        Delete user in database

        :param id_user: id of a user
        :type id_user: int

        :return: Boolean if the user was deleted in database
        :rtype: bool
        """
        user = User.query.get(id_user)
        if user is None:
            return None
        else:
            try:
                for offer in user.offers:
                    OfferManager.delete_offer(offer.id)
                dB.delete(user)
                dB.commit()
                return True
            except Exception:
                dB.rollback()
                return False
