#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot
import tensorflow as tf
import numpy as np

from SmartRecruiting_BackEnd import get_path
from SmartRecruiting_BackEnd.deeplearning.preprocess import preprocess
from SmartRecruiting_BackEnd.deeplearning.utils import ModelInitialisationError
from SmartRecruiting_BackEnd.deeplearning.utils import log, success

class Model(object):
    """
    Handle a Keras model. Several public methods (see method documentation to know more how to use) :

    * load : load the model from the directory [save_dirname]
    * build : generate the Keras model structure (layers)
    * train : train the Keras model
    * eval : eval the Keras model
    * predict : generate predictions for an offer
    """
    def __init__(self, save_dirname, word2vec, epochs):
        """
        Init an object containing mainly a keras model to train, eval and predict.

        :param save_dirname: directory where to save / load the Keras model
        :type save_dirname: str

        :param word2vec: Word2vec model (preprocessing model, see preprocess.py to know more about this model)
        :type word2vec: Word2Vec
        """
        self.save_dirname = save_dirname.strip()  # Clean directory path
        self.w2v_model = word2vec

        # Following parameters will be init with the _build method
        self.word_index = None
        self.embedding_matrix = None
        self.model = None
        self.epochs = epochs

        self._generate_word_index()
        self._generate_embedding_matrix()

        # Remove the last to normalize dirname
        if self.save_dirname[len(self.save_dirname) - 1] == "/":
            self.save_dirname = self.save_dirname[:len(self.save_dirname) - 1]

    ####################################################################################################################
    # Public methods   #################################################################################################
    ####################################################################################################################
    def load(self):
        """
        Load the model (and so init the model parameter) from the specified directory (see class parameters)
        """

        self.model = tf.keras.models.load_model(get_path("/data/model/final_model.h5"))

    def build(self, number_of_labels):
        """
        Build the Keras model structure (define the layers in our model)
        It's a multi-class classification, so the output are defined by [number_of_labels] parameter.

        .. warning::
           It's probably the most important methods to change to improve the model.
           The most "difficult" part is to find which layers to use and define layers parameters.

        :param number_of_labels:  output dimensions,
        :type number_of_labels: int
        """
        log("Build model...")
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Embedding(len(self.word_index) + 1, 100, weights=[self.embedding_matrix], input_length=400, trainable=False))
        model.add(tf.keras.layers.Dropout(0.1))
        model.add(tf.keras.layers.Conv1D(300, 3, padding='valid', activation='relu', strides=1))
        model.add(tf.keras.layers.GlobalMaxPool1D())
        model.add(tf.keras.layers.Dense(number_of_labels))
        model.add(tf.keras.layers.Activation('sigmoid'))

        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        self.model = model
        success("Build model : successful")

    def train(self, features, labels, save=True):
        """
        Train the Keras model with [features] and [label].

        :param features: features on which train the model
        :type features: list of str

        :param labels: array of hot vector that represent formation associated with features
                        So array of the same size of features lists. And each element is an hot vector
        :type labels: numpy array of numpy array of int (0 or 1)

        :param save: set to True to save the model (saved in the directory specify in the class parameters)
        :type save: bool
        """
        log("Train model...")
        if self.model is None:
            raise ModelInitialisationError("Model not build. Consider using build (and train) or load methods before.")

        save_filename = self.save_dirname + "/save_checkpoint_{epoch:03d}.h5"

        features = self._text_to_word_indexes(features)
        features = tf.keras.preprocessing.sequence.pad_sequences(features, maxlen=400, padding="post")
        callbacks = []
        if save:
            callbacks.append(tf.keras.callbacks.ModelCheckpoint(save_filename, verbose=1, period=2))
        self.model.fit(features, labels, epochs=self.epochs, callbacks=callbacks)
        if save:
            self.model.save("./data/model/final_model.h5")
        success("Train model : successful")

    def eval(self, features, labels):
        """
        Eval the accuracy and the loss of our model with [features] (and check the results with [labels]

        :param features: features on which evaluate the model
        :type features: list of str

        :param labels: array of hot vector that represent formation associated with features
                        So array of the same size of features lists. And each element is an hot vector
        :type labels: numpy array of numpy array of int (0 or 1)
        """
        log("Eval model...")
        if self.model is None:
            raise ModelInitialisationError("Model not build. Consider using build (and train) or load methods before.")

        print("Eval on " + str(len(features)) + " offers")
        features = self._text_to_word_indexes(features)
        features = tf.keras.preprocessing.sequence.pad_sequences(features, maxlen=400, padding="post")
        loss, acc = self.model.evaluate(features, labels)
        print('Test Accuracy: %f' % (acc * 100))
        success("Eval model : successful")

    def predict(self, feature):
        """
        Predict formations of the [feature]

        :param feature: offer to predict
        :type feature: str

        :return: dictionary of prediction {id_prediction: accuracy of the formation (mark)}
        :rtype: dict {int : int}
        """
        log("Predict feature ...")
        if self.model is None:
            raise ModelInitialisationError("Model not build. Consider using build (and train) or load methods before.")

        feature = self._text_to_word_indexes([feature])
        feature = tf.keras.preprocessing.sequence.pad_sequences(feature, maxlen=400, padding="post")[0]
        feature = np.expand_dims(feature, 0)

        predictions = self.model.predict(feature)[0]
        res = {}
        for i, prediction in enumerate(predictions):
            res[i] = prediction
        success("Predict feature : successful")
        return res

    ####################################################################################################################
    #  Private methods   ###############################################################################################
    ####################################################################################################################
    def _generate_word_index(self):
        """
        Init work_index class parameter with Word2Vec model vocabulary.
        `word_index` is a dictionary that contains a word as key and an index as value
        """
        if self.w2v_model is None:
            raise ModelInitialisationError("Word2Vec model not found")

        vocab = {}
        for i, word in enumerate(self.w2v_model.wv.vocab):
            vocab[word] = i
        self.word_index = vocab

    def _generate_embedding_matrix(self):
        """
        Init embedding_matrix class parameter It contains
        There are a direct link with the word_index.
        The index store in the word_index dictionary for each word
        """
        if self.word_index is None:
            raise ModelInitialisationError("Word index not initialized.")
        if self.w2v_model is None:
            raise ModelInitialisationError("Word2Vec model not found.")

        vocab_size = len(self.word_index) + 1
        embedding_matrix = np.zeros((vocab_size, 100))
        for word, index in self.word_index.items():
            embedding_matrix[index] = self.w2v_model[word]
        self.embedding_matrix = embedding_matrix

    def _text_to_word_indexes(self, texts):
        """
        Transform the [texts] to list of list of word descriptor

        :param texts: list of texts to transform
        :type texts: list of str
        """
        if self.word_index is None:
            raise ModelInitialisationError("Word index not initialized.")

        texts_res = []
        for text in texts:
            text_res = []
            text = preprocess.tokenize(text)
            for word in text:
                if word in self.word_index:
                    text_res.append(self.word_index[word])
            texts_res.append(text_res)
        return texts_res
