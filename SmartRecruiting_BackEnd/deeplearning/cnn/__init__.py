# encoding: utf-8
# -*- coding: utf-8 -*-
"""
Introduction
############

This module handle the Keras model to predict formation from an offer.

The class Model defined our Keras model and its methods (train, eval, predict, etc).

The file ``train.py`` create and train this model. So it's composed mainly of 1 file to load offers and
split them to train and eval the model.

TODO
####

It's difficult to choose the model's layers and parameters. So create a file to see the impacts of the model adjustment
is an interesting improvement :

* graph f(epoch) = accuracy or loss , to adjust epoch parameter
* keep in a file all Model summary (can easily be found with keras) and results associated (accuracy, loss) to keep the history of models and their performances

"""
