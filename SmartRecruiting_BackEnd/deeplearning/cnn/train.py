#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot
import tensorflow as tf

import SmartRecruiting_BackEnd.deeplearning.preprocess.preprocess as pretraitement
from SmartRecruiting_BackEnd.deeplearning.cnn.Model import Model
from SmartRecruiting_BackEnd import app


def get_offers(db_manager):
    """

    :return: list of offers preprocess (str with stemmed word not in stop list)
    :rtype: (list of str, list of int)
    """
    print("data recuperation...")

    predictions = db_manager.offer_manager.get_all_offers_with_field_in_base()
    x, y = [], []
    for p in predictions:
        x.append(str(p[0], 'utf-8'))
        y.append(p[1])

    return x, y


def train(db_manager):
    """
    Train a convolutional neuronal network and save it in a file in /runs

    :param: the database manager
    :type db_manager: DbManager
    """
    print("=====\nTRAIN\n=====")
    x, y = get_offers(db_manager)
    number_of_label = len(set(y)) + 1
    y = tf.keras.utils.to_categorical(y)

    train_partition = int(len(x) * 0.85)
    x_train, y_train = x[:train_partition], y[:train_partition]
    x_eval, y_eval = x[train_partition:], y[train_partition:]

    word2vec = pretraitement.load_word2vec()
    model = Model(save_dirname="./data/model", word2vec=word2vec, epochs=app.config["keras_model"]["epochs"])

    model.build(number_of_label)
    model.train(x_train, y_train, save=True)
    model.eval(x_eval, y_eval)
