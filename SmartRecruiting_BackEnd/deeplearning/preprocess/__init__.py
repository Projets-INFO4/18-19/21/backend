# encoding: utf-8
# -*- coding: utf-8 -*-
"""

Introduction
############
There are three files to handle the offers preprocessing:

* `preprocess.py` to handle preprocess operations on offers : transform the offers CSV file to list of offers which
  each offers is a list of word descriptor. A word descriptor is a vector of float numbers.
* `database_handler.py` to perform database operations : add or update the database (offers, fields, contacts)
* `utils.py` to visualize the words descriptor of our model vocabulary


TODO
#######################
- Implement unit testing on `database_handler.py` and complete the two last functions to do in `test_preprocess.py`
- Try to find (or build) a french texts corpus more recent (actually it's wikipedia pages from 2008)
- Review functions in `database_handler`


Updates (v2)
#######################

- Unit tests on `preprocess.py`. Unit tests `test_preprocess_all_and_add_to_database` and `test_recompute_all_descriptors` are not yet done because this functions calls database function, it's more complicated. For `test_preprocess_all_and_add_to_database`  we write some tests but it's commented ;
- Train word2vec model on Wikipedia corpus. You can see, below, the improvement thanks to this new training set. We used a list a words to plot to improve the graph readability:
- Add `utils.py` and a function to visualize our model vocabulary word representation ;
- Improve natural language tokenization on words (stemming, tokenization, punctuation). In particular we used NLTK library and regex to achieve a good tokenization ;
- Move database operations in `database_handler.py` file (now there are two files in preprocess, see above to know how it works) ;
- Remove duplicate code, improve readability and refactor code to improve the cognitive complexity ;
- Refactor style code to PEP8 and documenting functions. See contributing file or documentation to know more about our conventions.


Useful resources
########################

- [Text Preprocessing in Python](https://medium.com/@datamonsters/text-preprocessing-in-python-steps-tools-and-examples-bf025f872908)
- [Visualizing Word Vectors with t-SNE](https://www.kaggle.com/jeffd23/visualizing-word-vectors-with-t-sne)
- [word2vec google homepage](https://code.google.com/archive/p/word2vec/)
"""

from SmartRecruiting_BackEnd.deeplearning.preprocess import *
