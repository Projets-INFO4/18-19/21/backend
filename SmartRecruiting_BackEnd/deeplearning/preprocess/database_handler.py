#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot, Alicia
"""
TODO: Not yet review.
TODO/ finish to do the documentation
"""
import csv
from SmartRecruiting_BackEnd import app


def add_offer_to_database(db_manager, offer):
    """
    Add one preprocessed offer in the database

    :param db_manager: database manager
    :type: DatabaseManager

    :param offer: list of (text,descriptor,label)
    :rtype offer:
    """
    admin_id = db_manager.user_manager.get_one_admin().id
    name_field = offer[1]
    id_offer = add_an_offer(db_manager, offer, admin_id)
    if id_offer != -1:
        id_field = get_id_field(db_manager, name_field)
        if id_field != -1:
            db_manager.prediction_manager.add_prediction(100.0, id_field, id_offer, True)


def add_an_offer(db_manager, offer, id_admin):
    """
    Add a preprocessed offer in the database

    :param offer: (text,descriptor,label) the processed offer
    :type:

    :param db_manager: database manager
    :type db_manager: DatabaseManager

    :param id_admin;
    :type:

    :return the id of the newly created offer
    :rtype int
    """
    # Make title
    title = offer[0].split(' ')
    title = title[:5]
    title = ' '.join(title)

    _id = db_manager.offer_manager.add_offer_v2(title, offer[0], id_admin)
    return _id


def get_id_field(db_manager, name):
    """
    Add id of the named field or create the field and get the id if the field doesn't exist

    :param db_manager: database manager
    :type db_manager: DatabaseManager

    :param name: name of the field
    :type name: str

    :return: id of the offer if successful, -1 if not
    :rtype: int
    """
    field = db_manager.field_manager.get_field_by_name(name)
    if field:
        return field.id
    else:
        _id = db_manager.field_manager.add_field_v2(name, "", "")
        if _id != -1:
            return _id
        else:
            return -1


def set_field_information(db_manager, name, description, website):
    """
    Set information for the field name

    :param db_manager
    :param name : name of the field to update
    :param description
    :param website
    """
    _id = get_id_field(db_manager, name)
    db_manager.field_manager.update_field(_id, name, description, website)


def add_fields(db_manager):
    filename = app.config["fields_dataset"]
    with open(filename, encoding='utf-8', mode="r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            set_field_information(db_manager, row['name'], row['description'], row['website'])


def add_contact(db_manager, field_name, name, surname, role, email, phone):
    """
    Insert contact in the database

    :param db_manager
    :param field_name
    :param name
    :param surname
    :param role
    :param email
    :param phone
    """
    id_field = get_id_field(db_manager, field_name)
    db_manager.contact_manager.add_contact(name, surname, email, phone, role, id_field)


def add_contacts(db_manager):
    """

    :param db_manager: database_manager
    :type db_manager: DatabaseManager
    """
    filename = app.config["contacts_dataset"]
    with open(filename, encoding='utf-8', mode="r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            add_contact(db_manager, row['field'], row['name'], row['surname'], row['role'], row['email'], row['phone'])
