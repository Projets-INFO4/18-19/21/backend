#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot, Alicia
"""
Preprocessing of texts, using the removal of stop words and the Word2Vec to create descriptors.

Text descriptor format : python list (size = config) of numpy array (size = config)

Wikipedia FR corpus (2008) : http://redac.univ-tlse2.fr/corpora/wikipedia_en.html
Stats :
- Nb words : 284 472 742
- Nb lines : 37 654 166
"""
import csv
import re
import nltk
import numpy as np
from gensim.models import Word2Vec

from SmartRecruiting_BackEnd import app, get_path
from .utils import plot as plot_words
from .database_handler import add_offer_to_database, add_contacts, add_fields
from ..utils import stop_words, error


# Constants declarations
STOP_LIST = stop_words()
max_size = app.config['max_size_offers']  # The maximal size that a text should have.


def get_wikipedia_corpus():
    """
    Get content from the wikipedia corpus (filename specified in config file).
    This function read the corpus text file, and add lines to the result. A limit can be specified in the config
    file to not process all lines.

    :return: list containing lines from the wikipedia corpus
    :rtype: list of str
    """
    lines = []
    limit = app.config['word2vec']['maxlines_wiki_corpus']
    i = 0
    try:
        with open(app.config['word2vec']['filename_wiki_corpus'], encoding='iso-8859-1', mode='r') as f:
            line = f.readline()
            while line and i < limit:
                cleaned = tokenize(line)
                lines.append(cleaned)
                line = f.readline()
                i += 1
                if i % 10000 == 0:
                    print("Still " + str(limit - i) + " lines to process.", flush=True, end="\r")
    except Exception:
        error("Unable to get wikipedia corpus. Verify your corpus filename in your config file.")

    return lines


def build_word2vec(offers=None):
    """
    This function builds a Word2Vec neural network model. This model produces vector for each word in our vocabulary.
    Word2vec also build our vocabulary with our [sentences].

    This model will be trained on wikipedia corpus and offers :

    * from the offers csv file if offers is null
    * from the [offers] parameter else

    Offers csv filename and wikipedia corpus filename are defined in the config file.

    .. note::
        Word2vec model parameters can be changed in the config file.

    .. note::
        We have also add a padding words in the vocabulary to be able to pad texts with this word

    :param offers: (optional) offers used to train the model (else, offers from the CSV used)
    :type: list of dictionary with at least {'content: str }

    :return: Word2Vec model trained on [sentences]
    :rtype: Word2Vec
    """
    print("Build word2vec model ...")
    filename = app.config['offers_dataset']
    sentences = []
    if offers is None:
        with open(filename, encoding='utf-8', mode="r") as f:
            reader = csv.DictReader(f)
            for i, row in enumerate(reader):
                print('init offer ' + str(i + 1), end="\r", flush=True)
                text = row[app.config['feature']]  # Get the text from the initial offer
                sentences += [tokenize(text)]  # Sentences used to build the model's vocabulary

    else:
        for i, o in enumerate(offers):
            print('reinit offer ' + str(i + 1), end="\r", flush=True)
            text = o['content']
            sentences = sentences + [tokenize(text)]  # Sentences used to build the model's vocabulary
    print()  # Print a new line to not override lines below
    sentences.extend(get_wikipedia_corpus())
    print()  # Print a new line to not override lines below
    try:
        model = Word2Vec(sentences, size=app.config['word_vector_dim'], window=20, min_count=app.config['word2vec']['min_freq'], workers=4)
    except RuntimeError:
        error("Unable to train Word2Vec. Maybe you have not enough data to train the model, consider reduce min-freq in your config file.", critic=True)
    except Exception:
        error("Unexpected error while train Word2Vec.", critic=True)

    model.wv[app.config['padding_str']] = np.array([0] * app.config['word_vector_dim'])  # Add a descriptor for the padding word
    model.save(app.config['word2vec']['save_path'])
    if app.config['debug']['visualisation_w2c'] == 1:
        plot_words(model, max_words=app.config['debug']['max_word_to_plot'])
    return model


def load_word2vec():
    """
    Load the word2vec model from the file defined in the config file.
    If an error occurred (typically the file doesn't exists) we exit the execution.

    .. note::
        If want want to build the word2vec model consider using the build_word2vec method.

    :return: The word2vec model
    :rtype: Word2Vec
    """
    print("Load word2vec model ...")
    try:
        model = Word2Vec.load(get_path(app.config['word2vec']['save_path']))
        if app.config['debug']['visualisation_w2c'] == 1:
            plot_words(model, max_words=app.config['debug']['max_word_to_plot'])
        return model
    except Exception:
        error(message="Unable to load word2vec model.\nConsider using --word2vec to build the model.")
        exit(0)


def tokenize(text):
    """
    Function  to tokenize the [text]. Following steps are done to tokenize the [text] :
    1. Lowercase + remove whitespaces
    2. Keep only numbers
    3. Remove punctuations
    4. Tokenize the text into words
    5. For each word, if it is not in stopwords list, we add the stem of the word to the return result

    :param text: input text to tokenize
    :type text: str

    :return: list of words
    :rtype: list of str
    """
    stemmer = nltk.stem.snowball.FrenchStemmer()

    text = text.lower().strip()  # Lower + remove whitespaces
    text = re.sub(r'\d+', '', text)  # Remove numbers
    text = re.sub(r'[?|!|\'|"|#|.|,|)|(|\|/]', r'', text)  # Remove punctuation
    text = nltk.word_tokenize(text)  # tokenize
    res = []
    for word in text:  # Add stemmed words to result, except if this word is in the stop list
        if word not in STOP_LIST:
            res.append(stemmer.stem(word))
    return res


def preprocess(text, model):
    """
    Function to get a text descriptor from the [text]. Following steps are done :
    1. Tokenize text (see tokenize function)
    2. Normalization of the [text] lenght (padding or truncating)
    3. From the vocabulary of our Word2Vec model, we get the words representations

    :param text: the input text
    :type text: str

    :param model: word2vec model which contains words and words descriptor
    :type model: Word2Vec

    :return: Word representations (each word is a float numpy array)
    :rtype: list of numpy array
    """
    cleaned = tokenize(text)
    words = list(filter(lambda x: x in model.wv.vocab, cleaned))  # keep only words present in the model vocabulary

    # Trunc or padding text to normalize length
    if len(words) >= max_size:
        words = words[:max_size]
    else:
        words = words + [app.config['padding_str']] * (max_size - len(words))
    descriptor = [model.wv[w] for w in words]

    return descriptor


def preprocess_all_and_add_to_database(db_manager, file_name):
    """
    Function to preprocess the offers from a csv film [file_name] and add them in the database.
    See preprocess function to know more about the preprocess operations.

    :param file_name: name of the file containing the data to process
    :type file_name: str

    :param db_manager: database manager
    :rtype db_manager: DatabaseManager
    """
    with open(file_name, encoding='utf-8', mode="r") as f:
        reader = csv.DictReader(f)
        for i, row in enumerate(reader):
            print('Preprocess offer ' + str(i + 1), end="\r", flush=True)
            text = row[app.config['feature']]  # Get the text from the initial offer
            label = row[app.config['label']]
            add_offer_to_database(db_manager, (text, label))
    print()  # Print a new line to not override lines below


def init(db_manager, generate_w2c=False):
    """
    Function to initialize the model and the database.
    Has to be called before using the CNN model for the first time or if the csv files (offers, fields, contacts) has changed

    Populate the database with:
    - offers with their word descriptor
    - fields
    - contacts

    If generate_w2c if set to True, the word2vec model is build and train.

    If you run the app with runserver :
    - Use --init arg to launch this function
    - Use --word2vec arg to set this flag

    :param db_manager: database manager
    :type db_manager: DatabaseManager

    :param generate_w2c: (optional) True if word2vec model have to be build and train
    :type generate_w2c: bool
    """
    if generate_w2c:
        build_word2vec()

    # Put everything in the database
    filename = app.config['offers_dataset']
    preprocess_all_and_add_to_database(db_manager, filename)  # Preprocess and add offers to the database
    add_fields(db_manager)  # Add fields to the database
    add_contacts(db_manager)  # Add contacts to the database
