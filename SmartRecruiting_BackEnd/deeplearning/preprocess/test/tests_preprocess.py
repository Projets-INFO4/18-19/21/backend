#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot, Alicia
"""

"""
import sys
import os
import unittest
import numpy as np
from gensim.models import Word2Vec

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../..'))


from SmartRecruiting_BackEnd.deeplearning.preprocess.preprocess import build_word2vec, preprocess, tokenize, load_word2vec, get_wikipedia_corpus
from SmartRecruiting_BackEnd import app
from SmartRecruiting_BackEnd.deeplearning.utils import success, block_print, enable_print, todo


def set_test_config():
    app.config['word2vec']['maxlines_wiki_corpus'] = 100
    app.config['word2vec']['min_freq'] = 1
    app.config['word2vec']['save_path'] = "./data/preprocessing_model.test"
    app.config['offers_dataset'] = "./data/offers.test.csv"
    app.config['debug']['visualisation_w2c'] = 0
    app.config['TESTING'] = True


class TestPreprocess(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestPreprocess, self).__init__(*args, **kwargs)
        set_test_config()
        self.word_test = "offres"
        self.offers = [{'content': self.word_test}, {'content': "Usines usine USINES UsINe"}, {"content": "Du texte avec du vocabulaire informatioque, materiaux, elements, batiments"}]
        self.text = "Un (petit); texte de 4/5 mots, pour tester les fonctions."

    def test_tokenize(self):
        block_print()
        w1, w2, w3, w4 = tokenize("usine"), tokenize("Usine"), tokenize("UsinEs"), tokenize("Usines.")
        self.assertIsInstance(w1, list)
        w1, w2, w3, w4 = w1[0], w2[0], w3[0], w4[0]
        self.assertIsInstance(w1, str)
        self.assertEqual(w1, w2)
        self.assertEqual(w2, w3)
        self.assertEqual(w3, w4)
        s = tokenize(self.text)
        self.assertIsInstance(s, list)
        self.assertGreater(len(s), 0)
        for w in s:
            self.assertIsInstance(w, str)
            self.assertNotIn('4', w)
            self.assertNotIn('(', w)
            self.assertNotIn(')', w)
            self.assertNotIn('.', w)
            self.assertNotIn(',', w)
        enable_print()
        success("test_tokenize OK")

    def test_build_word2vec(self):
        block_print()
        # Test without parameters
        model1 = build_word2vec()
        model2 = build_word2vec(offers=self.offers)
        for model in [model1, model2]:
            self.assertIsNotNone(model)
            self.assertIsInstance(model, Word2Vec)
            self.assertIsInstance(model.wv.vocab, dict)
            self.assertGreater(len(model.wv.vocab.values()), 0)
            self.assertIn(app.config['padding_str'], model)

        # Special tests with the model with offers parameter
        self.assertGreater(len(model2.wv.vocab.values()), 5)
        test_word = tokenize(self.word_test)[0]
        self.assertIn(test_word, model2.wv.vocab.keys())
        enable_print()
        success("test_build_word2vec")

    def test_preprocess(self):
        block_print()
        model = build_word2vec()
        res = preprocess(self.text, model)
        self.assertIsInstance(res, list)
        self.assertGreater(len(res), 0)
        for d in res:
            self.assertIsInstance(d, np.ndarray)
            self.assertEqual(d.shape, (app.config['word_vector_dim'],))
        enable_print()
        success("test_preprocess")

    def test_load_word2vec(self):
        block_print()
        model1 = build_word2vec()
        model2 = load_word2vec()
        self.assertIsInstance(model2, Word2Vec)
        self.assertIsInstance(model2.wv.vocab, dict)
        self.assertEqual(model1.wv.vocab.keys(), model2.wv.vocab.keys())
        limit = 10
        i = 0
        for (k1, v1), (k2, v2) in zip(model1.wv.vocab.items(), model2.wv.vocab.items()):
            self.assertEqual(model1[k1].all(), model2[k2].all())
            i += 1
            if i > limit:
                break
        enable_print()
        success("test_load_word2vec")

    def test_preprocess_all_and_add_to_database(self):
        # block_print()
        # filename = app.config['offers_dataset']
        # preprocess_all_and_add_to_database(dbManager, filename)
        # offers = dbManager.get_all_offers()
        # model = load_word2vec()
        #
        # text_offers = []
        # descriptor_offers = []
        # for offer in offers:
        #     text_offers.append(offer['content'])
        #     descriptor_offers.append(offer['descriptor'])
        #
        # with open(filename, encoding='utf-8', mode="r") as f:
        #     reader = csv.DictReader(f)
        #     limit = 10  # Can be slow so we fix a limit
        #     for i, row in enumerate(reader):
        #         text = row[app.config['feature']]
        #         descriptor = preprocess(text, model)
        #         test_pass = False
        #         for text_offer, descriptor_offer in zip(text_offers, descriptor_offers):
        #             if text_offer == text:
        #                 if descriptor_to_string(descriptor) == descriptor_offer:
        #                     test_pass = True
        #                     break
        #         self.assertTrue(test_pass)
        #         if i >= limit:
        #             break
        # enable_print()
        todo("test_preprocess_all_and_add_to_database")

    def test_recompute_all_descriptors(self):
        block_print()
        enable_print()
        todo("test_preprocess_all_and_add_to_database")

    def test_get_wikipedia_corpus(self):
        block_print()
        lines = get_wikipedia_corpus()
        self.assertIsInstance(lines, list)
        self.assertNotEqual(len(lines), 0)
        enable_print()
        success("test_get_wikipedia_corpus")