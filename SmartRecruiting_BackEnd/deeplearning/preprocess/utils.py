#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# License : GNU General Public License
# Authors: Romain Guillot
"""

Utils function used in preprocess. Please see the documentation of each function.
"""
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE


def plot(model, max_words=100):
    """
    Inspired by kaggle tutorial : https://www.kaggle.com/jeffd23/visualizing-word-vectors-with-t-sne

    This functions plot in a 2D space the words descriptor of the words of the model vocabulary.

    Initially, words descriptor are vector of a higher dimension than 2 (see the config file to know exactly the
    dimension). This function use[t-SNE](https://en.wikipedia.org/wiki/T-distributed_stochastic_neighbor_embedding)
    machine learning algorithm.

    From wikipedia :
    > It (t-SNE) is a nonlinear dimensionality reduction technique well-suited for embedding high-dimensional data for
    visualization in a low-dimensional space of two or three dimensions

    :param model: word2vec model containing the vocabulary with words descriptor
    :type model: Word2Vec

    :param max_words: (optional) max words to plot
    :rtype max_words: int
    """
    labels, tokens = [], []

    # I use an index instead of a slice because vocab if a dict, so it's faster to use an index to break the loop
    # instead of casting the dict into a slicing object
    for i, word in enumerate(model.wv.vocab):
        tokens.append(model[word])
        labels.append(word)
        if i > max_words:
            break

    # words_to_test = ["informatique", "java", "python", "programmation", "developpement", "web", "internet", "langage",
    #                  "oracle", "linux", "windows", "eclipse", "rest", "mobile", "android", "ios", "smartphone", "css",
    #                  "html", "javascript", "django", "redhat", "spring", "learning", "matériaux", "beton", "tissu",
    #                  "cuisine", "alarme", "google", "windows", "photo", "construction", "chimie", "mathematique",
    #                  "ubuntu", "linux"]
    # words_to_test.extend(["microsoft", "amazon", "apple"])
    # for word in words_to_test:
    #     if word in model.wv.vocab :
    #         tokens.append(model[word])
    #         labels.append(word)

    tsne_model = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
    new_values = tsne_model.fit_transform(tokens)

    x, y = [], []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])

    plt.figure(figsize=(16, 16))
    for i in range(len(x)):
        plt.scatter(x[i], y[i])
        plt.annotate(labels[i], xy=(x[i], y[i]), xytext=(5, 2), textcoords='offset points', ha='right', va='bottom')

    plt.show()
