from SmartRecruiting_BackEnd import app, get_path
import sys
import os


class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    GRAY = '\033[90m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    ITALIC = "\x1B[3m"


def stop_words():
    return [word for line in open(get_path(app.config['stop_words']), encoding='utf-8', mode="r") for word in line.split()]


def error(message, critic=False):
    print(Colors.FAIL + "ERROR : " + message + Colors.ENDC)
    if critic:
        sys.exit(0)

def log(msg):
    print(Colors.GRAY + "LOG : " + msg + Colors.ENDC)

def success(msg):
    print(Colors.OKGREEN + "SUCCESS : " + msg + Colors.ENDC)


def todo(msg):
    print(Colors.WARNING + "TODO : " + msg + Colors.ENDC)


def block_print():
    """disable print"""
    sys.stdout = open(os.devnull, 'w')


def enable_print():
    """restore print"""
    sys.stdout.close()
    sys.stdout = sys.__stdout__


class ModelInitialisationError(Exception):
    pass
