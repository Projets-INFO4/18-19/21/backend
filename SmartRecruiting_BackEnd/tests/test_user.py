
import unittest

# add root directory to the path to import app
import sys
import os.path

sys.argv.append('-t') # Use test database for unit tests

for p in sys.argv:
    print(p)

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))


from SmartRecruiting_BackEnd import app

app.config['TESTING'] = True

from SmartRecruiting_BackEnd import db_manager
from SmartRecruiting_BackEnd.data import database
from SmartRecruiting_BackEnd.data import User
from SmartRecruiting_BackEnd.data import dbSession as dB
from SmartRecruiting_BackEnd.deeplearning.utils import success, block_print, enable_print, todo


class TestUser(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestUser, self).__init__(*args, **kwargs)
        print(app.config['TESTING'])
        database.init_db()

    def test_add_user(self):
        self.assertEqual(db_manager.user_manager.add_user('Quentin', 'Sibue', 'Etudiant', 'q@gmail.com', 'mdp1', 0), True)
        success('Add user.')

    def test_update_user(self):
        self.assertEqual(db_manager.user_manager.update_user(1, 'Name', 'Surname', 'Role', 'Email', 'Password', 0), True)
        self.assertEqual(db_manager.user_manager.get_user_by_id(1), {'id': 1, 'name': 'Name', 'surname': 'Surname', 'role': 'Role', 'email': 'Email', 'password': 'Password', 'is_admin': False})
        self.assertEqual(db_manager.user_manager.update_user(36, 'Name', 'Surname', 'Role', 'Email', 'Password', 0), None)
        success('Update user.')
