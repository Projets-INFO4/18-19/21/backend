===
CNN
===

.. automodule:: SmartRecruiting_BackEnd.deeplearning.cnn
   :members:

Code documentation
==================

``train.py``
************
.. automodule:: SmartRecruiting_BackEnd.deeplearning.cnn.train
  :members:

``Model.py``
************
.. automodule:: SmartRecruiting_BackEnd.deeplearning.cnn.Model
  :members:
