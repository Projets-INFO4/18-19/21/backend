====
Data
====


``models.py``
*************

.. automodule:: SmartRecruiting_BackEnd.data.models
   :members:


``database.py``
***************

.. automodule:: SmartRecruiting_BackEnd.data.models
   :members:

Managers
********

.. toctree::
   :maxdepth: 10
   :glob:

   managers
