========
Managers
========

``manager.py``
**************

.. autoclass:: SmartRecruiting_BackEnd.data.manager.DatabaseManager
   :members:

``offer_manager.py``
********************

.. autoclass:: SmartRecruiting_BackEnd.data.offer_manager.OfferManager
  :members:

``user_manager.py``
*******************

.. autoclass:: SmartRecruiting_BackEnd.data.user_manager.UserManager
   :members:

``prediction_manager.py``
*************************

.. autoclass:: SmartRecruiting_BackEnd.data.prediction_manager.PredictionManager
   :members:

``field_manager.py``
********************

.. autoclass:: SmartRecruiting_BackEnd.data.field_manager.FieldManager
   :members:

``contact_manager.py``
**********************

.. autoclass:: SmartRecruiting_BackEnd.data.contact_manager.ContactManager
   :members:
