==========
Preprocess
==========

.. automodule:: SmartRecruiting_BackEnd.deeplearning.preprocess
   :members:

Code documentation
==================

``preprocess.py``
*****************

.. automodule:: SmartRecruiting_BackEnd.deeplearning.preprocess.preprocess
  :members:


``database_handler.py``
***********************

.. automodule:: SmartRecruiting_BackEnd.deeplearning.preprocess.database_handler
  :members:


``utils.py``
************

.. automodule:: SmartRecruiting_BackEnd.deeplearning.preprocess.utils
   :members:
