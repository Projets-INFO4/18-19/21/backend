============
Contributing
============

Code conventions
================

Python files have to follow [PEP8](https://www.python.org/dev/peps/pep-0008/) style guide. You can test that project still follows PEP8 style guide with `flake8` :

* install ``flake8`` : ``python3 -m pip install flake8``
* Run ``flake8`` to the project root :

::

   cd <path to backend>
   ./flake8.sh


*If no files are printed, that's ok. Else, resolve problems.*

``flake8.sh`` have to be executable (``chmod a+x flake8.sh``)



Documentation
=============
Python docstrings follow reStructuredText syntax (`PEP287 <https://www.python.org/dev/peps/pep-0287/>`_).

This document is built with Sphinx with the theme ReadTheDocs. To install Sphynx and the theme :

.. code-block:: bash

    sudo apt-get install python3-sphinx
    pip3 install sphinx_rtd_theme


you can generate the html with the following command line :

.. code-block:: bash

    make -C docs html

The output (generated html files) is the directory ./public/

Unit testing
============

* In order to launch a specific test script, run the following command line :

.. code-block:: bash

    python3 -m unittest


* There is a SonarCloud set up for this project with these credentials:

    * Username:
    * Password:
