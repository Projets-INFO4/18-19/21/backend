.. SmartRecruiting back documentation master file, created by
   sphinx-quickstart on Sun Mar 10 10:27:17 2019.


SmartRecruiting back
====================

Documentation use reStructuredText, see `documention syntax <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_.

.. toctree::
   :maxdepth: 10
   :caption: Contents:

   install
   contributing
   documentation



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
