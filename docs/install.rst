============
Installation
============


Installation
============

Install mysql-server first and launch it:

.. code-block:: bash

    sudo apt-get update
    sudo apt-get install mysql-server
    sudo mysql

Then you have to make sure the nickname and password used are the same as the ones in *SmartRecruiting_BackEnd/data/database.py*:

.. code-block:: mysql

    UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user = 'root';
    FLUSH privileges;
    exit

There's no need to create tables, it will be done when you launch the application.

Then install Python3, Pip3 and Tkinter following these command lines:

.. code-block:: bash

    sudo apt-get install python3
    sudo apt-get install pip3
    sudo apt-get install python3-tk

Next, install the required packages in the requirements.txt file:

.. code-block:: bash

    pip3 install --no-cache-dir -I -r requirements.txt

You can verify your libraries by using the command *pip3 freeze*; a package may have the wrong version installed (especially Tensorflow, look for the version you need in the *requirements.txt* file) you can reinstall it like this:

.. code-block:: bash

    pip3 uninstall tensorflow
    pip3 install tensorflow==1.5

Tensorflow is an example there, you can just replace it by any package and their version number.

Finally, launch Python3 and download Punkt. It downloads a model for the tokenizer used in the project.

.. code-block:: python3

    import nltk
    nltk.download()

Go to *All Packages* and download *punkt*.

Required files
==============

* In order to launch the project, you have to add those 3 files in the *data* directory:

  * offers.csv
  * fields.csv
  * contacts.csv
* The files have to be in UTF-8 and the fields must be split with comas;
* The fields in the files are:

  * offers.csv : *Formation*, *Offre initiale*
  * fields.csv : *name*, *description*, *website*
  * contacts.csv : *name*, *surname*, *role*, *email*, *phone*, *field*
* The first lines in the files have to be the fields name.

Launch
======

* Launch the server:

.. code-block:: bash

    python3 runserver.py

* You can add one of those 2 parameters:

  * -i, --init : initializes the model and the database
  * -r, --reinit : initializes the model from the database

* You can, if you want, change the server port in *runserver.py*.
